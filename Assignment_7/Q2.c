#include <stdio.h>
#include <stdlib.h>

int main()
{
char str[100],c;
int i,frequency=0;
printf("Enter a string:   ");
fgets(str, sizeof(str), stdin);

printf("Enter a character to find frequency :");
scanf("%c",&c);

for(i=0;i < str[i] !='\0';i++)
{
    if(str[i]==c)
    {
        frequency++;
    }
}
printf("Frequency of %c is %d",c,frequency);
return 0;
}
