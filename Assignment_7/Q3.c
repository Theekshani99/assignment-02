#include <stdio.h>
#include <stdlib.h>


int main()
{

    int a[10][10], b[10][10], p[10][10]={0}, d[10][10]={0};
    int a_rows,a_columns,b_rows,b_columns;
    int r,c,k;
	printf("Enter no. of rows and columns in first matrix : ");
	scanf("%d%d",&a_rows,&a_columns);
	printf("Enter no. of rows and columns in second matrix : ");
	scanf("%d%d",&b_rows,&b_columns);
	if(a_rows!=b_rows || a_columns!=b_columns)
	{
		printf("warning!cannot add the first and second matrices,try again  ");
	return;
	}
	else if(a_columns!=b_rows)
	{
		printf("warning!cannot multiply the first and second matrices,try again ");
	return;
	}
	else
	{
		printf("Enter elements of first matrix : \n");
		for(r=0;r<a_rows;r++)
                {
			for(c=0;c<a_columns;c++)
                        {scanf("%d",&a[r][c]);}
                }
		printf("Enter elements of second matrix : \n");
		for(r=0;r<b_rows;r++)
                {
			for(c=0;c<b_columns;c++)
                        {scanf("%d", &b[r][c]);}
                }
                printf("\n");

		//matrix multiplication
		for(r=0;r<a_rows;r++){
			for(c=0;c<b_columns;c++)
                        {
				for(k=0;k<b_rows;k++)
                                {d[r][c] += a[r][k]*b[k][c];}
                        }
                }
		printf("Result of Matirx Multiplication:\n");
		for(r=0;r<a_rows;r++)
                {
			for(c=0;c<b_columns;c++)
				printf("%d ", d[r][c]);
			printf("\n");
		}
                //matrix addition
		for(r=0;r<a_rows;r++)
			for(c=0;c<a_columns;c++)
			p[r][c] = a[r][c]+b[r][c];

		printf("Result of Matirx Addition:\n");
		for(r=0;r<a_rows;r++)
                {
			for(c=0;c<a_columns;c++)
			printf("%d ", p[r][c]);
			printf("\n");
		}
        }

    return 0;
}
